/**
 * 
 */
package br.com.acme.reclamacao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.acme.reclamacao.Reclamacao;
import br.com.acme.reclamacao.service.ReclamacaoService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class ReclamacaoController {
	
	@Autowired
	private ReclamacaoService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/reclamacao")
	public ResponseEntity<Reclamacao> save(@RequestParam(name = "responsavel_id") Long responsavel_id,@RequestBody Reclamacao reclamacao) {
		
		this.service.gerarAviso(reclamacao, responsavel_id);
		
		return new ResponseEntity<Reclamacao>(reclamacao, HttpStatus.CREATED);
	}
}
