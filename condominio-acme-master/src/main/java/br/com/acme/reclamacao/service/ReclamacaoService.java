/**
 * 
 */
package br.com.acme.reclamacao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.reclamacao.Reclamacao;
import br.com.acme.reclamacao.repository.ReclamacaoRepository;
import br.com.acme.responsavel.Responsavel;
import br.com.acme.responsavel.repository.ResponsavelRepository;

/**
 * @author wilson
 *
 */
@Service
public class ReclamacaoService {
	
	@Autowired
	private ReclamacaoRepository reclamacaoRepository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Transactional
	public void gerarAviso(Reclamacao reclamacao, Long responsavel_id) {
		Responsavel responsavel = this.responsavelRepository.getOne(responsavel_id);
		reclamacao.setResponsavel(responsavel);
		this.reclamacaoRepository.save(reclamacao);
	}
}
