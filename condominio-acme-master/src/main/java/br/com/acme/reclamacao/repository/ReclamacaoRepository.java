/**
 * 
 */
package br.com.acme.reclamacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.reclamacao.Reclamacao;

/**
 * @author wilson
 *
 */
@Repository
public interface ReclamacaoRepository extends JpaRepository<Reclamacao, Long> {

}
