/**
 * 
 */
package br.com.acme.multas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.acme.condominio.Condominio;
import br.com.acme.unidade.Unidade;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author carlosfilho
 *
 */
@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@Table(name = "tb_multas")
public class Multa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String descricaoMulta;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private LocalDate dataMulta;
	
	@ManyToOne
	@JoinColumn(name = "id_unidade")
	private Unidade unidadeMulta;
	
	@ManyToOne
	@JoinColumn(name = "id_condominio")
	private Condominio condominoMulta;
	
	private BigDecimal valorMulta;
	
	public void setUnidade(Unidade unidade) {
		this.unidadeMulta = unidade;
	}
	
	public void setCondominio(Condominio condominio) {
		this.condominoMulta = condominio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricaoMulta() {
		return descricaoMulta;
	}

	public void setDescricaoMulta(String descricaoMulta) {
		this.descricaoMulta = descricaoMulta;
	}

	public LocalDate getDataMulta() {
		return dataMulta;
	}

	public void setDataMulta(LocalDate dataMulta) {
		this.dataMulta = dataMulta;
	}

	public Unidade getUnidadeMulta() {
		return unidadeMulta;
	}

	public void setUnidadeMulta(Unidade unidadeMulta) {
		this.unidadeMulta = unidadeMulta;
	}

	public Condominio getCondominoMulta() {
		return condominoMulta;
	}

	public void setCondominoMulta(Condominio condominoMulta) {
		this.condominoMulta = condominoMulta;
	}

	public BigDecimal getValorMulta() {
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}
	
	
}
