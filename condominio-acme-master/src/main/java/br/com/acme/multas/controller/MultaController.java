/**
 * 
 */
package br.com.acme.multas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.acme.condominio.Condominio;
import br.com.acme.multas.Multa;
import br.com.acme.multas.service.MultaService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class MultaController {
	
	@Autowired
	private MultaService service;
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/multas")
	public ResponseEntity<List<Multa>> list() {
		List<Multa> multas = this.service.list();
		return ResponseEntity.ok(this.service.list());
		
	}

}
