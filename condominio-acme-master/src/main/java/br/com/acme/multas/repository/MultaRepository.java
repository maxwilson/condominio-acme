/**
 * 
 */
package br.com.acme.multas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.multas.Multa;

/**
 * @author wilson
 *
 */
@Repository
public interface MultaRepository extends JpaRepository<Multa, Long> {
	public List<Multa> findBydescricaoMultaContaining(String descricaoMulta);
}
