/**
 * 
 */
package br.com.acme.multas.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.condominio.Condominio;
import br.com.acme.condominio.repository.CondominioRepository;
import br.com.acme.multas.Multa;
import br.com.acme.multas.repository.MultaRepository;
import br.com.acme.unidade.Unidade;
import br.com.acme.unidade.respository.UnidadeRepository;

/**
 * @author wilson
 *
 */
@Service
public class MultaService {
	
	@Autowired
	private MultaRepository repository;
	
	
	@Transactional(readOnly=true)
	public List<Multa> list() {
		return this.repository.findAll();
	}
	
}
