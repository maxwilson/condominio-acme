/**
 * 
 */
package br.com.acme.unidade.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.responsavel.Responsavel;
import br.com.acme.responsavel.repository.ResponsavelRepository;
import br.com.acme.unidade.Unidade;
import br.com.acme.unidade.respository.UnidadeRepository;

/**
 * @author wilson
 *
 */
@Service
public class UnidadeService {
	
	@Autowired
	private UnidadeRepository repository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Transactional
	public void save(Unidade unidade, Long responsavel_id) {
		Responsavel responsavel = this.responsavelRepository.getOne(responsavel_id);
		
		unidade.setResponsavelUnidade(responsavel);
		this.repository.save(unidade);
	}
	
	
	@Transactional(readOnly=true)
	public List<Unidade> list() {
		return this.repository.findAll();
	}
}
