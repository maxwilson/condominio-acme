/**
 * 
 */
package br.com.acme.unidade.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.unidade.Unidade;

/**
 * @author wilson
 *
 */
@Repository
public interface UnidadeRepository extends JpaRepository<Unidade, Long> {
	
}
