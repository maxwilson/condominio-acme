/**
 * 
 */
package br.com.acme.unidade.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.acme.unidade.Unidade;
import br.com.acme.unidade.service.UnidadeService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class UnidadeController {
	
	@Autowired
	private UnidadeService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/unidade")
	public ResponseEntity<Unidade> save(@RequestParam(name = "responsavel_id") Long responsavel_id,@RequestBody Unidade unidade) {
		
		this.service.save(unidade, responsavel_id);
		
		return new ResponseEntity<Unidade>(unidade, HttpStatus.CREATED);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/unidades")
	public ResponseEntity<List<Unidade>> list() {
		List<Unidade> unidade = this.service.list();
		return ResponseEntity.ok(this.service.list());
	}
}
