/**
 * 
 */
package br.com.acme.unidade;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.acme.multas.Multa;
import br.com.acme.responsavel.Responsavel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author carlosfilho
 *
 */
@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@Table(name = "tb_unidade")
public class Unidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_responsavel")
	private Responsavel responsavelUnidade;
	
	private String numeroUnidade;
	
	private String blocoUnidade;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeMulta")
	private Set<Multa> multasUnidade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Responsavel getResponsavelUnidade() {
		return responsavelUnidade;
	}

	public void setResponsavelUnidade(Responsavel responsavelUnidade) {
		this.responsavelUnidade = responsavelUnidade;
	}

	public String getNumeroUnidade() {
		return numeroUnidade;
	}

	public void setNumeroUnidade(String numeroUnidade) {
		this.numeroUnidade = numeroUnidade;
	}

	public String getBlocoUnidade() {
		return blocoUnidade;
	}

	public void setBlocoUnidade(String blocoUnidade) {
		this.blocoUnidade = blocoUnidade;
	}

	public Set<Multa> getMultasUnidade() {
		return multasUnidade;
	}

	public void setMultasUnidade(Set<Multa> multasUnidade) {
		this.multasUnidade = multasUnidade;
	}

	
	
}
