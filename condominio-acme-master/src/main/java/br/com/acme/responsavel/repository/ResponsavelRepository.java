/**
 * 
 */
package br.com.acme.responsavel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.responsavel.Responsavel;

/**
 * @author wilson
 *
 */
@Repository
public interface ResponsavelRepository extends JpaRepository<Responsavel, Long>{
	public List<Responsavel> findByNomeContaining(String nome);
}
