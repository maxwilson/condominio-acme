/**
 * 
 */
package br.com.acme.gerar.multa.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.condominio.Condominio;
import br.com.acme.condominio.repository.CondominioRepository;
import br.com.acme.multas.Multa;
import br.com.acme.multas.repository.MultaRepository;
import br.com.acme.unidade.Unidade;
import br.com.acme.unidade.respository.UnidadeRepository;

/**
 * @author wilson
 *
 */
@Service
public class GerarMultaService {
	
	@Autowired
	private MultaRepository multaRepository;
	
	@Autowired
	private CondominioRepository condominioRepository;
	
	@Autowired
	private UnidadeRepository unidadeRepository;
	
	@Transactional
	public void gerarMulta(Multa multa, Long unidade_id, Long condominio_id) {
		
		Unidade unidade = this.unidadeRepository.getOne(unidade_id);
		Condominio condominio = this.condominioRepository.getOne(condominio_id);
		
		Multa multado = multa;
		multado.setCondominio(condominio);
		multado.setUnidade(unidade);
		
		this.multaRepository.save(multado);
		
	}
}
