/**
 * 
 */
package br.com.acme.gerar.multa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.acme.gerar.multa.service.GerarMultaService;
import br.com.acme.multas.Multa;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class MultarController {
	
	@Autowired
	private GerarMultaService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/gerar/multa/unidade")
	public ResponseEntity<Multa> save(@RequestParam(name = "unidade_id") Long unidade_id, 
			@RequestParam(name="condominio_id") Long condominio_id, 
			@RequestBody Multa multa) {
		
		this.service.gerarMulta(multa, unidade_id, condominio_id);
		
		return new ResponseEntity<Multa>(multa, HttpStatus.CREATED);
	}
}
