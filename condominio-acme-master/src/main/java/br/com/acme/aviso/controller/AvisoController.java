/**
 * 
 */
package br.com.acme.aviso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.acme.aviso.Aviso;
import br.com.acme.aviso.service.AvisoService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class AvisoController {

	@Autowired
	private AvisoService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/gerar/aviso/unidade")
	public ResponseEntity<Aviso> save(@RequestParam(name = "unidade_id") Long unidade_id,  
			@RequestBody Aviso aviso) {
		
		//this.service.gerarAviso(aviso, unidade_id);
		
		return new ResponseEntity<Aviso>(this.service.gerarAviso(aviso, unidade_id), HttpStatus.CREATED);
	}
}
