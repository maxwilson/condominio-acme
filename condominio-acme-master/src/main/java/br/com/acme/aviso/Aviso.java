/**
 * 
 */
package br.com.acme.aviso;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.acme.unidade.Unidade;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author carlosfilho
 *
 */
@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@Table(name = "tb_avisos")
public class Aviso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String descricaoAviso;
	
	@ManyToOne // Muitos 'this.avisos' para uma UNIDADE
	@JoinColumn(name = "id_unidade")
	private Unidade unidadeAvisos;
	
	public void setUnidade(Unidade unidade) {
		this.unidadeAvisos = unidade;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricaoAviso() {
		return descricaoAviso;
	}

	public void setDescricaoAviso(String descricaoAviso) {
		this.descricaoAviso = descricaoAviso;
	}

	public Unidade getUnidadeAvisos() {
		return unidadeAvisos;
	}

	public void setUnidadeAvisos(Unidade unidadeAvisos) {
		this.unidadeAvisos = unidadeAvisos;
	}
	
	
}
