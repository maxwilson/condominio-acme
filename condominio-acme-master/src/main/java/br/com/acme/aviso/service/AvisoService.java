/**
 * 
 */
package br.com.acme.aviso.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.aviso.Aviso;
import br.com.acme.aviso.repository.AvisoRepository;
import br.com.acme.unidade.Unidade;
import br.com.acme.unidade.respository.UnidadeRepository;

/**
 * @author wilson
 *
 */
@Service
public class AvisoService {
	
	@Autowired
	private AvisoRepository repository;
	
	@Autowired
	private UnidadeRepository unidadeRepository;
	
	@Transactional
	public Aviso gerarAviso(Aviso aviso, Long unidade_id) {
		
		Unidade unidade = this.unidadeRepository.getOne(unidade_id);
		
		aviso.setUnidade(unidade);
		
		return this.repository.save(aviso);
		
	}
}
