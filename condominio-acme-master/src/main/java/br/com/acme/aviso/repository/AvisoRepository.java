/**
 * 
 */
package br.com.acme.aviso.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.aviso.Aviso;

/**
 * @author wilso
 *
 */
@Repository
public interface AvisoRepository extends JpaRepository<Aviso, Long> {
	
	
}
