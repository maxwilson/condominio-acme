/**
 * 
 */
package br.com.acme.condominio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.acme.condominio.Condominio;
import br.com.acme.condominio.service.CondominioService;

/**
 * @author wilson
 *
 */
@Controller
@RequestMapping("/api")
public class CondominioController {
	
	@Autowired
	private CondominioService service;
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/condominio")
	public ResponseEntity<Condominio> save(@RequestBody Condominio condominio) {
		this.service.save(condominio);
		
		return new ResponseEntity<Condominio>(condominio, HttpStatus.CREATED);
	}
}
