/**
 * 
 */
package br.com.acme.condominio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.acme.condominio.Condominio;

/**
 * @author wilson
 *
 */
@Repository
public interface CondominioRepository extends JpaRepository<Condominio, Long> {
	public List<Condominio> findByNomeContaining(String nome);
}
