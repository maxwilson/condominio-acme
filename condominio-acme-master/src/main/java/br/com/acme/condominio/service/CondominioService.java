/**
 * 
 */
package br.com.acme.condominio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.acme.condominio.Condominio;
import br.com.acme.condominio.repository.CondominioRepository;

/**
 * @author wilson
 *
 */
@Service
public class CondominioService {
	@Autowired
	private CondominioRepository repository;
	
	@Transactional
	public void save(Condominio condominio) {
		this.repository.save(condominio);
	}
}
